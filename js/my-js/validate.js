//Kiem tra rong
function kiemTraRong(value, idError) {
  if (value.length == "") {
    document.getElementById(idError).innerText =
      "Trường này không được để rỗng";
    // document.getElementById(idError).style.display = "block";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    // document.getElementById(idError).style.display = "none";
    return true;
  }
}

// Kiem tra tai Khoan Nv da ton tai
function kiemTraTaiKhoanNv(idNv, listNv, idError) {
  var index = listNv.findIndex(function (nv) {
    return nv.taiKhoan == idNv;
  });
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    // document.getElementById(idError).style.display = "none";
    return true;
  } else {
    document.getElementById(idError).innerText =
      "Tài khoản Nhân viên đã tồn tại";
    // document.getElementById(idError).style.display = "block";
    return false;
  }
}

// Kiem tra ky So
function kiemTraSo(idNv, idError) {
  const regNumber = /^\d+$/;
  // var reg = new RegExp('^[0-9]$');
  var isNumber = regNumber.test(idNv);
  if (!isNumber) {
    document.getElementById(idError).innerText =
      "Tài khoản Nhân viên phải là số";
    // document.getElementById(idError).style.display = "block";
    return false;
  } else {
    if (idNv.length >= 4 && idNv.length <= 6) {
      document.getElementById(idError).innerText = "";
      //   document.getElementById(idError).style.display = "none";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Tài khoản Nhân viên chứa từ 4 đến 6 ký số";
      //   document.getElementById(idError).style.display = "block";
      return false;
    }
  }
}

//Kiem tra chuoi Ky tu
function kiemTraKyTu(tenNv, idError) {
  const regCharactersAndSpaces = /^[A-Za-z\s]*$/;
  var isLetter = regCharactersAndSpaces.test(tenNv);
  if (!isLetter) {
    document.getElementById(idError).innerText = "Tên Nhân Viên Phải là chữ";
    // document.getElementById(idError).style.display = "block";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    // document.getElementById(idError).style.display = "none";
    return true;
  }
}

//Kiem tra email
function kiemTraEmail(email, idError) {
  const regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regEmail.test(email);
  if (!isEmail) {
    document.getElementById(idError).innerText =
      "Nhập email đúng định dạng xxx@xxx.xxx";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

//Kiem tra Mat Khau
function kiemTraMatKhau(matKhau, idError) {
  if (matKhau.length >= 6 && matKhau.length <= 10) {
    var haveNumber = matKhau.match(/\d/g, "");
    var haveUppercaseCharacter = matKhau.match(/[A-Z]/g, "");
    var haveregSpecialChar = matKhau.match(
      /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g,
      ""
    );
    if (
      haveNumber == null ||
      haveUppercaseCharacter == null ||
      haveregSpecialChar == null
    ) {
      document.getElementById(idError).innerText =
        "Mật Khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  } else {
    document.getElementById(idError).innerText =
      "Mật Khẩu phải chứa ít nhất 6 đến 10 ký tự";
    return false;
  }
}
//Kiem tra ngay lam
function kiemTraNgay(ngayLam, idError) {
  var regDate =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
  var isDate = regDate.test(ngayLam);
  if (!isDate) {
    document.getElementById(idError).innerText =
      "Nhập đúng định dạng MM/DD/YYYY hoặc chọn ngày từ ô Lịch bên cạnh";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

//Kiem tra muc luong Co ban
function kiemTraLuong(luong, idError) {
  if (luong * 1 >= 1e6 && luong * 1 <= 2e7) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText =
      "Lương cơ bản từ 1,000,000 vnđ đến 20,0000,000 vnđ";
    return false;
  }
}

//kiem tra chuc vu
function kiemTraChuVu(chucVu, idError) {
  if (chucVu != "Chọn chức vụ") {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Chọn một chức vụ phù hợp";
    return false;
  }
}
// kiem tra gio lam
function kiemTraGioLam(gioLam, idError) {
  if (gioLam * 1 >= 80 && gioLam * 1 <= 200) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText =
      "Số giờ làm trong tháng từ 80 giờ đến 200 giờ";
    return false;
  }
}
