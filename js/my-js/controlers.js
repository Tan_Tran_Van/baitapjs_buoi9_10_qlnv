//
function formInputDsnv() {
  var taiKhoan = document.getElementById("tknv").value.trim();
  var name = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  //
  var ngayLam = document.getElementById("datepicker").value.trim();
  var luongCB = document
    .getElementById("luongCB")
    .value.trim()
    .split(",")
    .join("");
  var chucVu = document.getElementById("chucvu").value.trim();
  var gioLam = document.getElementById("gioLam").value.trim();

  var newNhanVien = new NhanVien(
    taiKhoan,
    name,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return newNhanVien;
}

//
function renderDsnv(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var currentNV = dsnv[i];
    contentHTML += `<tr>
        <td>${currentNV.taiKhoan}</td>
        <td>${currentNV.name}</td>
        <td>${currentNV.email}</td>
        <td>${currentNV.ngayLam}</td>
        <td>${currentNV.chucVu}</td>
        <td>${currentNV.tinhTongLuong()} vnđ</td>
        <td>${currentNV.tinhXepLoai()}</td>
        <td>
        <button class="btn btn-danger my-1" onclick="xoaNhanVien(${
          currentNV.taiKhoan
        })">Xoá</button>
        <button class="btn btn-success my-1" onclick="suaNhanVien(${
          currentNV.taiKhoan
        })" data-toggle="modal" data-target="#myModal">Sửa</button>
        </td>
        </tr>`;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
//
function resetForm(idReset) {
  document.getElementById(idReset).reset();
}
//
function disabledInput(idDis) {
  document.getElementById(idDis).disabled = true;
}
//
function enabledInput(idEna) {
  document.getElementById(idEna).disabled = false;
}

//showThongTinNhanVienLenInput
function showThongTinNhanVienLenInput(nhanVien) {
  document.getElementById("tknv").value = nhanVien.taiKhoan;
  document.getElementById("name").value = nhanVien.name;
  document.getElementById("email").value = nhanVien.email;
  document.getElementById("password").value = nhanVien.matKhau;
  //
  document.getElementById("datepicker").value = nhanVien.ngayLam;
  document.getElementById("luongCB").value = nhanVien.luongCB
    .toString()
    .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  document.getElementById("chucvu").value = nhanVien.chucVu;
  document.getElementById("gioLam").value = nhanVien.gioLam;
}

// in thong bao
function xuatThongBao() {
  var spanThongbao = document.querySelectorAll(".sp-thongbao");
  for (var i = 0; i < spanThongbao.length; i++) {
    if (spanThongbao[i].innerText != "") {
      spanThongbao[i].style.display = "block";
    } else {
      spanThongbao[i].style.display = "none";
    }
  }
}
//reset thong bao
function resetxuatThongBao() {
  var spanThongbao = document.querySelectorAll(".sp-thongbao");
  for (var i = 0; i < spanThongbao.length; i++) {
    spanThongbao[i].innerText = "";
    spanThongbao[i].style.display = "none";
  }
}

//validate
function kiemTraValid(nhanVien) {
  var isValid = true;
  //ten
  isValid &=
    kiemTraRong(nhanVien.name, "tbTen") && kiemTraKyTu(nhanVien.name, "tbTen");
  //email
  isValid &=
    kiemTraRong(nhanVien.email, "tbEmail") &&
    kiemTraEmail(nhanVien.email, "tbEmail");
  //matKhau
  isValid &=
    kiemTraRong(nhanVien.matKhau, "tbMatKhau") &&
    kiemTraMatKhau(nhanVien.matKhau, "tbMatKhau");
  //ngay Lam
  isValid &=
    kiemTraRong(nhanVien.ngayLam, "tbNgay") &&
    kiemTraNgay(nhanVien.ngayLam, "tbNgay");
  //luongCB
  isValid &=
    kiemTraRong(nhanVien.luongCB, "tbLuongCB") &&
    kiemTraLuong(nhanVien.luongCB, "tbLuongCB");
  //Chuc vu
  isValid &=
    kiemTraRong(nhanVien.chucVu, "tbChucVu") &&
    kiemTraChuVu(nhanVien.chucVu, "tbChucVu");
  //Gio Lam
  isValid &=
    kiemTraRong(nhanVien.gioLam, "tbGiolam") &&
    kiemTraGioLam(nhanVien.gioLam, "tbGiolam");
  //
  return isValid;
}
