function NhanVien(
  _taiKhoan,
  _name,
  _email,
  _matKhau,
  _ngayLam,
  _luongCB,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.name = _name;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongCB = _luongCB;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Giám đốc") {
      return (this.luongCB * 3).toLocaleString();
    } else if (this.chucVu == "Trưởng phòng") {
      return (this.luongCB * 2).toLocaleString();
    } else if (this.chucVu == "Nhân viên") {
      return (this.luongCB * 1).toLocaleString();
    }
  };
  this.tinhXepLoai = function () {
    if (this.gioLam * 1 >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam * 1 >= 176) {
      return "Giỏi";
    } else if (this.gioLam * 1 >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
