var danhSachNhanVien = [];

var DSNV = "DSNV";

//lay dsnv tu localStorage
var dataJson = localStorage.getItem(DSNV);
if (dataJson) {
  dataRaw = JSON.parse(dataJson);
  danhSachNhanVien = dataRaw.map(function (nhanVien) {
    return new NhanVien(
      nhanVien.taiKhoan,
      nhanVien.name,
      nhanVien.email,
      nhanVien.matKhau,
      nhanVien.ngayLam,
      nhanVien.luongCB,
      nhanVien.chucVu,
      nhanVien.gioLam
    );
  });
  ////
  renderDsnv(danhSachNhanVien);
}

//luu dsnv xuong localStorage
function saveLocalStorage(dsnv) {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dsnvJson);
}

//them nhan vien
document.getElementById("btnThemNV").onclick = function () {
  var newNhanVien = formInputDsnv();
  //validate
  var isValid = kiemTraValid(newNhanVien);
  //tai khoan
  isValid &=
    kiemTraRong(newNhanVien.taiKhoan, "tbTKNV") &&
    kiemTraTaiKhoanNv(newNhanVien.taiKhoan, danhSachNhanVien, "tbTKNV") &&
    kiemTraSo(newNhanVien.taiKhoan, "tbTKNV");

  if (isValid) {
    danhSachNhanVien.push(newNhanVien);
    saveLocalStorage(danhSachNhanVien);
    renderDsnv(danhSachNhanVien);
    resetForm("formInput");
  } else {
    xuatThongBao();
  }
};
//xoa nhan vien
function xoaNhanVien(taiKhoan) {
  index = danhSachNhanVien.findIndex(function (nhanVien) {
    return nhanVien.taiKhoan == taiKhoan;
  });
  if (index == -1) return;
  danhSachNhanVien.splice(index, 1);
  saveLocalStorage(danhSachNhanVien);
  renderDsnv(danhSachNhanVien);
}
//Sua thong tin nhan vien

function suaNhanVien(taiKhoan) {
  index = danhSachNhanVien.findIndex(function (nhanVien) {
    return nhanVien.taiKhoan == taiKhoan;
  });
  if (index == -1) return;
  showThongTinNhanVienLenInput(danhSachNhanVien[index]);
  disabledInput("tknv");
  disabledInput("btnThemNV");
  enabledInput("btnCapNhat");
  resetxuatThongBao();
}
//cap nhat nhan vien
document.getElementById("btnCapNhat").onclick = function () {
  var nhanVienEdit = formInputDsnv();
  //validate
  var isValid = kiemTraValid(nhanVienEdit);
  if (isValid) {
    var index = danhSachNhanVien.findIndex(function (nhanVien) {
      return nhanVien.taiKhoan == nhanVienEdit.taiKhoan;
    });
    if (index == -1) return;
    danhSachNhanVien[index] = nhanVienEdit;
    saveLocalStorage(danhSachNhanVien);
    renderDsnv(danhSachNhanVien);
    enabledInput("tknv");
    enabledInput("btnThemNV");
    resetForm("formInput");
  } else {
    xuatThongBao();
  }
};

////Search xepLoai
document.getElementById("btnTimNV").onclick = function () {
  var noiDung = document.getElementById("searchName").value.toLowerCase();
  var nhanVien = danhSachNhanVien.filter(function (nv) {
    var xepLoai = nv.tinhXepLoai().toLowerCase();
    return xepLoai.indexOf(noiDung) > -1;
  });
  renderDsnv(nhanVien);
};

//reset form input, span thong bao
document.getElementById("btnThem").onclick = function () {
  resetForm("formInput");
  resetxuatThongBao();
  disabledInput("btnCapNhat");
  enabledInput("tknv");
  enabledInput("btnThemNV");
};
document.getElementById("btnDong").onclick = function () {
  resetForm("formInput");
  resetxuatThongBao();
  disabledInput("btnCapNhat");
  enabledInput("tknv");
  enabledInput("btnThemNV");
};

//kiem tra luong va them dau "," vao phan nghin (dieu kien luong la so nguyen, vd: 1,000,000; khong dung duoc voi so thap phan: 1,000,000.543)
document.getElementById("luongCB").oninput = function () {
  var luong = document.getElementById("luongCB").value.split(",").join("");
  document.getElementById("luongCB").value = (luong * 1)
    .toString()
    .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  //   document.getElementById("luongCB").value = (luong * 1).toLocaleString("en");
};
